# Solutions to [Advent Of Code](http://adventofcode.com/)

Advent of Code is a series of small programming puzzles for a variety of skill levels. They are self-contained and are just as appropriate for an expert who wants to stay sharp as they are for a beginner who is just learning to code. Each puzzle calls upon different skills and has two parts that build on a theme.

I am sharing my sources which are written in Java 8 to anyone who is interested. I also adapting the functional way to solving the question.
Some of my answers might not have the optimal performance but I wanted to keep the code readable rather than compact and fast.

## How to get the answer

Running the java class as Java Application. The result will be printed to the console logs.

## Author

Timmy Wong (thcathy@gmail.com)

###### Remark for Day 19 second part

The solution of this part is tricky. It is only applicable based on specific input analysis. i.e. This may not correct if the input pattern changed.
For more information, you can read the discussion from [reddit](https://www.reddit.com/r/adventofcode/comments/3xflz8/day_19_solutions/)
